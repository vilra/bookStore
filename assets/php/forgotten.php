<?php

    $data['status'] = false;

    $in = file_get_contents('php://input');

    if(empty($in) && json_decode($in)->{'email'}) {
        return false;
    }

    $data['status'] = false;
    $email = json_decode($in)->{'email'};
    $token = mb_strtoupper(strval(bin2hex(openssl_random_pseudo_bytes(16)))).time();

    $query = $db->query("SELECT id, name FROM users WHERE email = '$email'");
    $row = $query->fetch(PDO::FETCH_ASSOC);

    if(!$row) {
        return false;
    }

    $id = $row['id'];
    $name = $row['name'];
    $create_at = time();

    $count = $db->query("SELECT count_update FROM redeem_password WHERE user_id = $id ORDER BY count_update DESC LIMIT 1");

    $row = $count ? $count->fetch(PDO::FETCH_ASSOC) : 0;
    $count_update = $row ? $row['count_update'] : 0;

    $sendToRedeemPassword = $db->query("INSERT INTO redeem_password (user_id, token, create_at, count_update) VALUES($id, '$token', $create_at, '$count_update')");

    if(!$sendToRedeemPassword) {
        return false;
    }

    $data['status'] = true;
    $data['id'] = $id;
    $data['name'] = $name;
    $data['email'] = $email;
    $data['token'] = $token;


