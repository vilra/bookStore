<?php require 'config.php';

    if(in_array($_SERVER['REQUEST_METHOD'], array('GET'))) {
        $id = isset($_REQUEST['userId']) ? $_REQUEST['userId'] : 0;
        $limit = $_REQUEST['limit'];
        $offset = $_REQUEST['offset'];
        $where = $id ? "WHERE b.owner_id = $id" : '';
        $queryId = $db->query("SELECT id as lastBookId FROM books ORDER BY id DESC LIMIT 1");
        $lastId = $queryId->fetch(PDO::FETCH_ASSOC);

        $query = $db->query("
            SELECT b.*, COUNT(bp.id) as count_pages,
            (SELECT bi.file_name FROM book_images as bi WHERE is_cover = 1 AND b.id = bi.book_id) as cover,
            (SELECT COUNT(books.id) FROM books) as allBooks
            FROM books as b
            LEFT JOIN book_pages as bp ON bp.book_id = b.id 
            $where
            GROUP BY b.id
            LIMIT $limit 
            OFFSET $offset
        ");

        $collection = $query->rowCount() ? $query->fetchAll(PDO::FETCH_OBJ) : 0;
    }

    echo json_encode($collection);

