<?php require 'config.php';

    define('USER_PATH', __DIR__ . '/../users/');

    $files = $_FILES;
    $post = $_POST;
    $message['message'] = 'Fail to created book';

    $title = trim($post['title']);
    $genre = trim($post['genre']);
    $author = trim($post['author']);
    $create_at = time();
    $owner_id = trim($post['id']);
    $currentUser = 'user-' . $owner_id;
    $pages = $post['pages'];
    $orderNo = 1;

    $a = explode(' ' , $title);
    $b = implode('-', $a);

    $queryBook = $db->query("INSERT INTO books (owner_id, title, genre, author, create_at) VALUES($owner_id, '$title', '$genre', '$author', $create_at)");

    if($queryBook) {
        $id = $db->lastInsertId();
        mkdir("../users/" . $currentUser . '/books/' . $title . DIRECTORY_SEPARATOR, 0777, true) || 'Fail to create folder...';

        foreach($pages as $page) {
            $queryPage = $db->query("INSERT INTO book_pages (book_id, page, order_no) VALUES($id, '$page', $orderNo)");
            $orderNo++;
        }

        foreach($files['images']['tmp_name'] as $key => $tmp_name ) {
            $file_name = $files['images']['name'][$key];
            $file_size = $files['images']['size'][$key];
            $file_tmp = $files['images']['tmp_name'][$key];
            $file_type = $files['images']['type'][$key];

            $array = explode('.', $file_name);
            $lastIndex = count($array) - 1;
            $extension = $array[$lastIndex];
            $newName = time() . '-' . $key . '.' . $extension;
            $uploadPath = USER_PATH . $currentUser . '/books/' . $title . DIRECTORY_SEPARATOR . $newName;
            $isUploaded = move_uploaded_file($file_tmp, $uploadPath);

            $queryImages = $db->query("INSERT INTO book_images (book_id, file_name, file_size, file_type) VALUES ($id, '$newName', '$file_size', '$file_type')");
        }

        if($queryImages AND $queryBook) {
            $message['message'] = 'Congratulation, the book was successfully created!';
        }
    }

    echo json_encode($message);


