<?php require 'config.php';

    if(in_array($_SERVER['REQUEST_METHOD'], array('PUT', 'POST'))) {
        $in = file_get_contents('php://input');
        $message['message'] = '';

        if(!empty($in)) {
            $post_data = json_decode($in);
            $id = $_SESSION['user_id'];
            $token = $_SESSION['token'];
            $count = $_SESSION['count_update'];
            $password = md5($post_data->{'password'});
            $update_at = time();

            if($token) {
                $db->query("UPDATE redeem_password SET token = '', update_at = $update_at, count_update = $count WHERE token = '$token'");
                $db->query("UPDATE users SET password = '$password' WHERE id = '$id'");

                unset($_SESSION);

                $message['message'] = 'Successfully updated password';
            }
            else {
                $message['message'] = 'Fail to updated password';
            }
        }
        echo json_encode($message);
    }

    if(in_array($_SERVER['REQUEST_METHOD'], array('GET'))) {
        if(!empty($_REQUEST)) {
            $token = $_REQUEST['token'];

            $query = $db->query("SELECT user_id, token, count_update FROM redeem_password WHERE token = '$token'");
            $match = $query->fetch(PDO::FETCH_ASSOC);

            if($match) {
                $_SESSION['token'] = $token;
                $_SESSION['user_id'] = $match['user_id'];
                $_SESSION['count_update'] = $match['count_update'] + 1;

                $message['message'] = 'Successfully';
            }
            else {
                $message['message'] = 'Fail';
            }
        };

        echo json_encode($message);
    }