<?php
    require 'config.php';
    require '../../vendor/autoload.php';

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    define(USER_PATH, '../users/user-');

    $in = isset(json_decode(file_get_contents('php://input'))->{'gender'});
    $in ? require_once 'userCredentials.php' : require_once 'forgotten.php';

    $email = $data['email'];
    $name = $data['name'];
    $id = $data['id'];
    $token = $data['token'];
    $password = isset($data['password']) ? $data['password'] : '';
    $data['status'] = false;

    if($in) {
        mkdir(USER_PATH . $id . '/books', 0777, true);
    }

    $mail = new PHPMailer(true);

    try {
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->SMTPAutoTLS = false;
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->Username = 'vurjil@gmail.com';
        $mail->Password = '0885923777Daniel';

        $mail->setFrom('vurjil@gmail.com', 'Daniel');
        $mail->addAddress($email, $name);

        $mail->isHTML(true);

        if($password !== '') {
            $mail->Subject = 'Signup | Verification';
            $mail->Body = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>User Registration</title>
                <meta content="EMAIL_SUBJECT" />
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            </head>
            <body>
                <p>
                    Thanks for signing up ' . $name . '.
                    Your account has been created, you can login with
                    the following credentials after you have activated
                    your account by pressing the url below.
                </p>
                <p>
                    ------------------------
                </p>
                <p>
                    Name: ' . $name . '
                </p>
                <p>
                    Password: ' . $password . '
                </p>
                <p>
                    ------------------------ ' . '
                </p>
                <p>
                    Please <a href="http://localhost/bookStore_PHPBB/#verify/token/' . $token . '"> click this link </a> to activate your account.
                </p>
             </body>
        </html>';
        }
        else {
            $mail->Subject = 'Password | Redeem';
            $mail->Body = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Generate New Password</title>
                <meta content="EMAIL_SUBJECT" />
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            </head>
            <body>
                <p>
                    Please <a href="http://localhost/bookStore_PHPBB/#newPassword/token/' . $token . '"> click this link </a> to generate new password.
                </p>
             </body>
        </html>';
        }

        if($mail->send()) {
            if($password !== '') {
                $data = [
                    'id' => $id,
                    'status' =>  true,
                    'message' => 'Your account has been made, <br /> please verify it by clicking the activation link that has been send to your email.'
                ];
            }
            else {
                $data = [
                    'status' =>  true,
                    'message' => 'Message was successfully send.'
                ];
            }
        }
    }
    catch(Exception $e) {
        $data = [
            'status' =>  false,
            'message' => 'Message could not be sent.'
        ];
    }

    echo json_encode($data);