<?php session_start();

    $dbhost="localhost";
    $dbuser="root";
    $dbpass="";
    $dbname="bb_book_store";

    $db = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, array(
        PDO::ATTR_PERSISTENT => true
    ));

    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

