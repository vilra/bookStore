template = {
    templates: {},
    loadTemplates: function(names, callback) {
        var self = this;
        var loadTemplate = function(index) {
            var name = names[index];

            console.log('Loading template: ' + name);

            $.get('templates/' + name + '.html', function(data) {
                self.templates[name] = data;
                index++;

                index < names.length ? loadTemplate(index) : callback();
            });
        };
        loadTemplate(0);
    },

    get: function(name) {
        return this.templates[name];
    }
};