App.Views.Verify = App.Views.BaseView.extend({
    tagName: 'div',

    initialize: function(token) {
        this.template = _.template($('#verifyTemplate').html());
        this.render();
        this.verify(token);

        this.model.bind("change", this.render, this);
    },

    events: {
        'click a.active': 'relocate'
    },

    verify: function(token) {
        var self = this;

        this.model.fetch({
            traditional: true,
            data: token,
            success: function(results) {
                if(results.get('error') === '') {
                    self.$el.find('div.container').append('<h2 class="verifyMessage">' + results.get('message') + '</h2><hr>');
                }
                else {
                    self.relocate();
                }
            },
            error: function(errors) {
                self.$el.append('<h2 class="verifyMessage">' + errors.get('error') + '</h2><hr>');
            }
        });
    }
});