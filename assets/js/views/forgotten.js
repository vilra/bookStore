
    App.Views.Forgotten = App.Views.BaseCredential.extend({
        urlRoot: 'assets/php/sendEmail.php',
        modelInstance: new App.Models.ForgottenModel,
        tagName: 'div',
        initialize: function() {
            this.template = _.template($('#forgottenTemplate').html());
            this.render();
        },
        events: _.extend({}, App.Views.BaseCredential.prototype.events, {
            'submit form#redeemPasswordForm': 'checkEmailExists'
        }),
        redeem: function(self, status) {
            $('div.controlError').html('');

            this.model.set({
                email: status
            });

            this.model.save(null, {
                url: this.urlRoot,
                wait: true,
                success: this.callSuccess,
                error: this.callError.bind(this, self.model.get('message'))
            });

            this.clearForm('redeemPasswordForm')
        },
        checkEmailExists: function(e) {
            e.preventDefault();
            $('div.controlError').html('');

            var self = this;
            var email = $('form #forEmail').val();
            var email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if(email_filter.test(email)) {
                this.model.fetch({
                    data: {
                        email: email
                    },
                    check: true
                });
                this.model.on("check_email", function(status) {
                    if(status === 'undefined') {
                        $('input[type=email]')
                            .closest('.controls')
                            .find('div.controlError')
                            .html('Email was not found in database');

                        return false;
                    }
                    self.redeem(self, status);
                });
            }
        }
    });