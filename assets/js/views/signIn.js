
    App.Views.SignIn = App.Views.BaseCredential.extend({
        urlRoot: 'assets/php/sendEmail.php',
        modelInstance: new App.Models.SignInModel,
        tagName: 'div',
        events: _.extend({}, App.Views.BaseCredential.prototype.events, {
            'submit form' : 'onSubmit',
            'change input[type=radio]': 'collectGender'
        }),
        initialize: function() {
            Backbone.Validation.bind(this, {
                model: this.model,
                forceUpdate: true,
                valid: function (view, attr, selector) {
                    var val = view.$('[name=' + attr + ']');
                    var group = val.closest('.controls');

                    group.find('div.controlError').empty();
                },
                invalid: function(view, attr, error,  selector) {
                    var val = view.$('[name=' + attr + ']');
                    var group = val.closest('.controls');

                    group.find('div.controlError').html(error);
                }
            });

            this.template = _.template($('#signInTemplate').html());
            this.render();
        },
        collectGender: function(e) {
            this.gender = $(e.currentTarget);
        },
        onSubmit: function(e) {
            e.preventDefault();
            $('div.controlError').html('');

            this.model.set({
                name: $('form #name').val(),
                email: $('form #email').val(),
                password: $('form #password').val(),
                confPassword: $('form #confPassword').val(),
                gender: this.gender ? this.gender.val() : ''
            });

            if(this.model.isValid(true)) {
                this.model.save(null, {
                    url: this.urlRoot,
                    wait: true,
                    success: this.callSuccess,
                    error: this.callError
                });

                this.clearForm('addUser')
            }
        }
    });


