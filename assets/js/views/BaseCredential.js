

    App.Views.BaseCredential = App.Views.BaseView.extend({
        urlRoot: null,
        modelInstance: null,
        events: {
            'blur form#addUser input[type=email]': 'checkEmailExists'
        },
        initialize: function() {
            this.model.bind('change', this.render, this);
        },
        checkEmailExists: function(e) {
            e.preventDefault();

            var email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var email = $('form #email').val();

            if(email_filter.test(email)) {

                this.modelInstance.fetch({
                    data: {
                        email : email
                    },
                    check : true
                });
                this.modelInstance.on("check_email", function(status) {
                    if(status === 'undefined') {
                        $('input[type=email]')
                            .addClass('green')
                            .closest('.controls')
                            .find('div.controlError')
                            .html('');
                    }
                    else {
                        $('input[type=email]')
                            .removeClass('green')
                            .closest('.controls')
                            .find('div.controlError')
                            .html('Email already exist in database');
                    }
                })
            }
        }
    });