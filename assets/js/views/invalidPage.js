

    App.Views.InvalidPage = App.Views.BaseCredential.extend({
        initialize: function() {
            this.template = _.template($('#invalidPageTemplate').html());
            this.render();
        }
    });