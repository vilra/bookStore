
    App.Views.AddBook = App.Views.BaseView.extend({
        tagName: 'div',

        initialize: function() {
            this.i = 1;
            this.template = _.template($('#addBookTemplate').html());

            Backbone.Validation.bind(this, {
                model: this.modelInstance,
                forceUpdate: true,
                valid: function (view, attr, selector) {
                    var val = view.$('[name=' + attr + ']');
                    var group = val.closest('.controls');

                    group.find('div.controlError').empty();
                },
                invalid: function(view, attr, error,  selector) {
                    var val = view.$('[name=' + attr + ']');
                    var group = val.closest('.controls');

                    group.find('div.controlError').html(error);
                }
            });
        },

        events: {
            'click input[name=createBook]': 'createBook',
            'click form#newBooks input': 'checkPage',
            'click a.deleteX':  'deleteX'
        },

        checkPage: function(e) {
            var that = $(e.currentTarget);
            var remove = $('form#newBooks input[name="removePage"]');

            if(that.attr('name') === 'addPage' ) {
                this.addPage(remove);
            }
            else if(that.attr('name') === 'removePage') {
                this.removePage(remove);
            }
        },

        addPage: function(remove) {
            remove.removeAttr('disabled');

            var div = $(
                '<div id="' + ++this.i + '" style="z-index: ' + this.i + '  " class="holdPage">' +
                '<span>' + ' Page ' + this.i + '</span>' +
                '</div>'
            );

            div.append(
                '<textarea class="textarea" name="pages[]"></textarea>' +
                '<a href="" style="z-index: ' + this.i + '  " class="deleteX">x</a>' +
                '<input type="hidden" name="page' + this.i + '" />'
            );
            $('.holdAllPage').append(div);

        },

        removePage: function(remove) {
            --this.i;
            $('.holdAllPage').children().last().remove();

            if(this.i === 1) {
                remove.attr('disabled', 'disabled');
            }
        },

        deleteX: function(e) {
            e.preventDefault();

            --this.i;
            $(e.currentTarget).parent().remove();
        },

        goToPage: function() {
            console.log('key up');
        },

        createBook: function(e) {
            e.preventDefault();
            this.clearErrorField();

            var that = this;
            var formData = new FormData();
            var picture = $('input[name="userFiles"]')[0].files;

            for(var o = 0; o < picture.length; o++){
                formData.append("images[]", picture[o]);
            }

            $('.holdPage').each(function() {
                formData.append('pages[]', $(this).children('.textarea').val());
            });

            formData.append('title', this.collectData('newBooks', 'title'));
            formData.append('genre', this.collectData('newBooks', 'genre'));
            formData.append('author', this.collectData('newBooks', 'author'));
            formData.append('id', this.user.id);

            $.ajax({
                url: 'assets/php/setBook.php',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,

                success: function(data) {
                    that.model.set(that.parseData(data));
                    that.callSuccess(that.model);
                },

                error: function(data) {
                    that.model.set(that.parseData(data));
                    that.callError(that.model);
                },

                complete: this.relocate()
            });
        }
    });