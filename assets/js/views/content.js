
    App.Views.Content = App.Views.BaseView.extend({
        tagName: 'div',
        offset: 0,
        limit: 5,
        totalPage: 0,
        loadMore: 1,

        initialize: function() {
            this.collection.on('sync', this.render, this);
            this.allBooks = new App.Views.Books({collection: this.collection});
            this.template = _.template($('#contentTemplate').html());
            this.isLoggedIn();
            this.checkPage();
        },

        events: {
            "click #logout": "onLogoutClick",
            'click .profileImage ': 'activateFormChangeAvatar',
            'change #imageUpload': 'changeAvatar'
        },

        checkPage: function() {
            var that = this;
            var $window = $(window);

            document.addEventListener('scroll', function() {

                if($window.scrollTop() + $window.height() >= $(document).height() && that.loadMore - 1 < that.totalPage) {
                    that.checkForExistingBooks(that)
                }
            });
        },

        checkForExistingBooks: function(that) {
            this.collection.fetch({
                traditional: true,
                data: {
                    userId: this.user.id ? this.user.id : '',
                    offset: this.offset,
                    limit: this.limit
                },

                complete: function() {
                    that.render();
                    that.offset += that.limit;
                    that.loadMore++;
                    that.totalPage = Math.ceil(that.collection.models[0].get('allBooks') / that.limit);

                    $('.holdBooks').append(that.allBooks.el);
                }
            });
        },

        onLogoutClick: function(e) {
            e.preventDefault();

            this.user.save(null,{
                url: 'assets/php/logout.php',
                success: this.isLoggedIn,
                complete: this.relocate.bind(this, 'login')
            });
        },

        activateFormChangeAvatar: function(e) {
            e.preventDefault();

            $('#imageUpload').click();
        },

        changeAvatar: function() {
            var formData = new FormData();
            var that = this;

            formData.append('image', $('input[name="profilePhoto"]')[0].files[0]);
            formData.append('id', this.user.id);

            $.ajax({
                url: 'assets/php/setProfilePicture.php',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,

                success: function(data) {
                    that.user.set({avatar: that.parseData(data).avatar});
                },

                error: function(error) {
                    console.log(error);
                }
            });
        }
    });