    App.Views.Login = App.Views.BaseCredential.extend({
        urlRoot: 'assets/php/users.php',
        modelInstance: new App.Models.LoginModel,
        tagName: 'div',
        events: {
            'submit': 'login'
        },

        initialize: function() {
            this.template = _.template($('#loginTemplate').html());
            this.isLoggedIn();
        },

        login: function(e) {
            e.preventDefault();
            $('div.controlError').html('');

            var attrs = {
                email: $('form#loginForm input#logEmail').val(),
                password: $('form#loginForm input#logPassword').val()
            };

            this.model.set(attrs);

            if(this.model.isValid(true)) {
                this.model.fetch({
                    traditional: true,
                    data: attrs,
                    success: this.saveUser(this.user),
                    error: this.callError(this.user)
                }, {
                    check: true
                }, this);

                this.relocate();
            }
        }
    });