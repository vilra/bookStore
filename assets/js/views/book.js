

    App.Views.Book = Backbone.View.extend({
        tagName: 'div',
        className: 'bookInfo',

        initialize: function() {
            this.template = _.template($('#holdBooksTemplate').html());
        },

        render: function() {
            this.$el.html(this.template(this.model.toJSON()));

            return this;
        }
    });