

    App.Views.Books = Backbone.View.extend({
        tagName: 'div',

        initialize: function() {
            this.collection.on('add', this.addBook, this);
        },

        render: function() {
            this.collection.each(this.addBook, this);
        },

        addBook: function(model) {
            var book = new App.Views.Book({
                model: model
            });

            this.$el.append(book.render().el);
        }
    });