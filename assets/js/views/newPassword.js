
    App.Views.NewPassword = App.Views.BaseView.extend({
        urlRoot: 'assets/php/newPassword.php',
        tagName: 'div',
        initialize: function(token) {
            this.template = _.template($('#newPasswordTemplate').html());
            this.render();
            this.verify(token);
        },
        events: {
            'submit form#newPasswordForm ': 'submit'
        },
        submit: function(e) {
            e.preventDefault();

            var self = this;
            var attrs = {
                password: $('form input[name=newPassword]').val(),
                confPassword: $('form input[name=confNewPassword]').val()
            };
            var data = this.model.set(attrs);

            if(data.isValid(true)) {
                data.save(data, {
                    url: this.urlRoot,
                    wait: true,
                    success: function(response) {
                        self.callSuccess(response.get('message'))
                    },
                    error: function(response) {
                        self.callError(response.get('message'))
                    }
                });
            }
        },
        verify: function(token) {
            var self = this;

            this.model.fetch({
                traditional: true,
                data: token,
                success: function(response) {
                    if(response.get('message') === 'Fail') {
                        self.relocate();
                    }
                },
                error: function(response) {
                    self.callError(response.get('message'))
                }
            });
        }
    });