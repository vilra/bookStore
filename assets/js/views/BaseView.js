

    App.Views.BaseView = Backbone.View.extend({
        user: new App.Models.User,
        modelInstance: null,

        initialize: function() {
            this.template = _.template($('#contentTemplate').html());
        },

        isLoggedIn: function(callOnComplete) {
            var that = this;
            var request = {
                url: 'assets/php/loginUser.php',
                complete: function() {
                    that.render();
                }
            };

            if(!callOnComplete && _.isFunction(this.checkForExistingBooks)) {
                request.complete = function() {
                    that.checkForExistingBooks(that);
                };
            }

            this.user.fetch(request);
        },

        saveUser: function(model) {
            this.user.set({
                id: model.id,
                name: model.get('name'),
                email: model.get('email'),
                gender: model.get('gender'),
                cover_image: model.get('cover_image')
            });
        },

        render: function() {
            $(this.el).html(this.template());

            return this;
        },

        onClose: function(){
            this.model.unbind("change", this.render);
        },

        clearForm: function(attr) {
            $('#' + attr)[0].reset();
        },

        clearErrorField: function() {
            $('div.controlError').html('');
        },

        collectData: function(form, attr) {
            return $('form#' + form + ' input[name=' + attr + ']').val();
        },

        callSuccess: function(data) {
            var div = $('div.message');
            var message = data.get('message') ? data.get('message') : data;

            div.html(message).slideDown(500);
            setTimeout(function () {
                div.slideUp(1000);
            }, 4000);
        },

        callError: function(data) {
            var message = $('div.message');

            message.html(data.get('message')).slideDown(500);
            setTimeout(function () {
                message.slideUp(1000);
            }, 4000);
        },

        relocate: function(data) {
            var param = data !== undefined ? data : '';

            router.navigate(param, {
                trigger: true
            });
        },
        parseData: function(data) {
            return JSON.parse(data);
        }
    });