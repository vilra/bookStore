
    App.Models.SignInModel = Backbone.Model.extend({
        urlRoot: 'assets/php/checkEmail.php',
        defaults: {
            id: null,
            name: '',
            email: '',
            password: '',
            confPassword: '',
            gender: '',
            message: '',
            error: ''
        },
        parse: function(response) {
            var self = this;

            for(var i in response) {
                if(response.hasOwnProperty(i)) {
                    if(i === 'email') {
                        self.trigger('check_' + i, response[i]);
                    }
                    else {
                        return response;
                    }
                }
            }
        },
        validation: {
            name: {
                required: true,
                rangeLength: [4, 12],
                msg: 'Please enter your Name.'
            },
            password: {
                required: true,
                rangeLength: [3, 8],
                msg: 'Your password must be at least 3 characters in length.'
            },
            email: {
                required: true,
                pattern: 'email',
                msg: 'Please enter a valid email'
            },
            confPassword: {
                equalTo: 'password',
                msg: 'The passwords does not match'
            },
            gender: {
                required: true,
                msg: 'Gender can\'t be empty.'
            }
        },
        initialize: function() {
            _.extend(Backbone.Model.prototype, Backbone.Validation.mixin);
        }
    });