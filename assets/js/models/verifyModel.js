
    App.Models.VerifyModel = Backbone.Model.extend({
        urlRoot: 'assets/php/verify.php',
        defaults: {
            token: '0',
            message: '',
            error: ''
        }
    });