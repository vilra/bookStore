
    App.Models.NewPassword = Backbone.Model.extend({
       urlRoot: 'assets/php/newPassword.php',
        defaults: {
            id: null,
            password: '',
            confPassword: '',
            token: '',
            message: '',
            error: ''
        },
        validation: {
            password: {
                required: true,
                rangeLength: [3, 8],
                msg: 'Your password must be at least 3 characters in length.'
            },
            confPassword: {
                equalTo: 'password',
                msg: 'The passwords does not match'
            }
        },
        initialize: function() {
            _.extend(Backbone.Model.prototype, Backbone.Validation.mixin);
        }
    });