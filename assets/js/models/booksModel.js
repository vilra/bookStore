
    App.Models.BooksModel = Backbone.Model.extend({
        urlRoot: 'assets/php/showAllData.php',
        defaults: {
            id: null,
            title: null,
            genre: null,
            author: null,
            count_pages: null,
            cover: null,
            create_at: null,
            update_at: null,
            message: null,
            allBooks: null
        }
    });