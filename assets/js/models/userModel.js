

    App.Models.User = Backbone.Model.extend({
        url: 'assets/php/logout.php',
        defaults: {
            id: null,
            name: null,
            email: null,
            gender: null,
            token: null,
            avatar: null
        }
    }, {
        USER_PATH_TO_FOLDER: 'assets/users/user-'
    });