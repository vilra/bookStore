
    App.Models.ForgottenModel = Backbone.Model.extend({
        urlRoot: 'assets/php/checkEmail.php',
        defaults: {
            idForgottenPassword: null,
            name: null,
            email: null,
            token: null,
            message: '',
            error: ''
        },
        parse: function(response) {
            var self = this;

            for(var i in response) {
                if(response.hasOwnProperty(i)) {
                    if(i === 'email') {
                        self.trigger('check_' + i, response[i]);
                    }
                    else {
                        return response;
                    }
                }
            }
        },
        validation: {
            email: {
                required: true,
                pattern: 'email',
                msg: 'Please enter a valid email'
            }
        },
        initialize: function() {
            _.extend(Backbone.Model.prototype, Backbone.Validation.mixin);
        }
    });