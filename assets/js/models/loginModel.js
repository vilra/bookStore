
    App.Models.LoginModel = Backbone.Model.extend({
        urlRoot: 'assets/php/loginUser.php',
        defaults: {
            email: null,
            password: null,
            message: '',
            error: ''
        },

        validation: {
            password: {
                required: true,
                rangeLength: [3, 8],
                msg: 'Your password must be at least 3 characters in length.'
            },

            email: {
                required: true,
                pattern: 'email',
                msg: 'Please enter a valid email'
            }
        },

        initialize: function() {
            _.extend(Backbone.Model.prototype, Backbone.Validation.mixin);
        }
    });