
    App.Models.AddBookModel = Backbone.Model.extend({
        url: 'assets/php/setBook.php',
        defaults: {
            title: null,
            genre: null,
            author: null,
            pages: null,
            images: null,
            message: null,
            error: null
        },
        validation: {
            title: {
                required: true,
                msg: 'Please enter Book Title.'
            },
            genre: {
                required: true,
                msg: 'Please enter Book Genre.'
            },
            author: {
                required: true,
                msg: 'Please enter Book Author.'
            }
        },
        // setFromFile: function(file) {
        //     var reader = new FileReader();
        //     var that = this;
        //
        //     reader.onload = (function(f) {
        //         return function(e) {
        //             that.set({filename: f.name});
        //             that.set({data: e.target.result});
        //         };
        //     })(file);
        //
        //     reader.readAsDataURL(file);
        // },
        initialize: function() {
            _.extend(Backbone.Model.prototype, Backbone.Validation.mixin);
        }
    });

