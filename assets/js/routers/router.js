
    Backbone.View.prototype.close = function() {
        this.remove();
        this.unbind();
        if(this.onClose){
            this.onClose();
        }
    };

    App.Router = Backbone.Router.extend({
        currentView: null,
		routes: {
			'': 'home',
			'signIn': 'signIn',
            'login': 'login',
			'addBook': 'addBook',
            'afterLogin': 'afterLogin',
            'verify/token/:id':'verify',
            'forgotten': 'forgotten',
            'newPassword/token/:id': 'newPassword',
            '*other': 'invalidPage'
		},

        initialize: function() {
            Backbone.history.start();
        },

        home: function() {
            this.loadView(new App.Views.Content({
                collection: new App.Collections.BooksCollection
            }));
		},

		signIn: function() {
            this.loadView(new App.Views.SignIn({
                model: new App.Models.SignInModel
            }));
		},

        login: function() {
            this.loadView(new App.Views.Login({
                model: new App.Models.LoginModel
            }));
        },

        addBook: function() {
            this.loadView(new App.Views.AddBook({
                model: new App.Models.AddBookModel
            }));
		},

        verify: function(token) {
            this.loadView(new App.Views.Verify({
                model: new App.Models.VerifyModel,
                token: token
            }));
        },

        forgotten: function() {
            this.loadView(new App.Views.Forgotten({
                model: new App.Models.ForgottenModel
            }));
        },

        newPassword: function(token) {
            this.loadView(new App.Views.NewPassword({
                model: new App.Models.NewPassword,
                token: token
            }));
        },

        invalidPage: function() {
            this.loadView(new App.Views.InvalidPage());
        },

        loadView: function(view) {
            var currentView = this.currentView;

            currentView ? currentView.close() : currentView = view;
            $("#contend").html(currentView.render().el);
        }
	});

    var router = new App.Router();








